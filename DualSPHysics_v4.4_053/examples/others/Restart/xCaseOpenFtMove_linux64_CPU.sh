  #!/bin/bash 
  
  fail () { 
    echo Execution aborted. 
    read -n1 -r -p "Press any key to continue..." key 
    exit 1 
  }

# "name" and "dirout" are named according to the testcase

export name=CaseOpenFtMove
export dirout=${name}_out
export diroutdata=${dirout}/data

# "executables" are renamed and called from their directory

export dirbin=../../../bin/linux
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dirbin}
export gencase="${dirbin}/GenCase4_linux64"
export dualsphysicscpu="${dirbin}/DualSPHysics4.4CPU_linux64"
export dualsphysicsgpu="${dirbin}/DualSPHysics4.4_linux64"
export boundaryvtk="${dirbin}/BoundaryVTK4_linux64"
export partvtk="${dirbin}/PartVTK4_linux64"
export partvtkout="${dirbin}/PartVTKOut4_linux64"
export measuretool="${dirbin}/MeasureTool4_linux64"
export computeforces="${dirbin}/ComputeForces4_linux64"
export isosurface="${dirbin}/IsoSurface4_linux64"
export flowtool="${dirbin}/FlowTool4_linux64"
export floatinginfo="${dirbin}/FloatingInfo4_linux64"

# RUN FIRST PART OF SIMULATION (0-1 seconds)

# "dirout" is created to store results or it is removed if it already exists

if [ -e ${dirout} ]; then rm -r ${dirout}; fi
mkdir ${dirout}
if [ $? -ne 0 ] ; then fail; fi
mkdir ${diroutdata}

# CODES are executed according the selected parameters of execution in this testcase

# Executes GenCase4 to create initial files for simulation.
${gencase} ${name}_Def ${dirout}/${name} -save:all
if [ $? -ne 0 ] ; then fail; fi

# Executes DualSPHysics to simulate first second.
${dualsphysicscpu} ${dirout}/${name} ${dirout} -dirdataout data -svres -tmax:1
if [ $? -ne 0 ] ; then fail; fi

# Executes post-processing tools...
export dirout2=${dirout}/particles
${partvtk} -dirin ${diroutdata} -savevtk ${dirout2}/PartFluid -onlytype:-all,+fluid
if [ $? -ne 0 ] ; then fail; fi

${partvtkout} -dirin ${diroutdata} -savevtk ${dirout2}/PartFluidOut -SaveResume ${dirout2}/_ResumeFluidOut
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/floatings
${boundaryvtk} -loadvtk ${dirout}/${name}__Actual.vtk -motiondata ${diroutdata} -savevtkdata ${dirout2}/Floatings.vtk
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/fluidslices
${isosurface} -dirin ${diroutdata} -saveslice ${dirout2}/Slices
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/floatinginfo
${floatinginfo} -dirin ${diroutdata} -savedata ${dirout2}/FloatingMotion 
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/height
${measuretool} -dirin ${diroutdata} -points ${name}_PointsHeights.txt -onlytype:-all,+fluid -height -savevtk ${dirout2}/PointsHeight -savecsv ${dirout2}/_Height
if [ $? -ne 0 ] ; then fail; fi

# RESTART SIMULATION AND RUN LAST PART OF SIMULATION (1-4 seconds)

export olddiroutdata=${diroutdata}
export dirout=${name}_restart_out
export diroutdata=${dirout}/data

# "redirout" is created to store results of restart simulation

if [ -e ${dirout} ]; then rm -r ${dirout}; fi
mkdir ${dirout}
if [ $? -ne 0 ] ; then fail; fi
mkdir ${diroutdata}

# CODES are executed according the selected parameters of execution in this testcase

# Executes GenCase4 to create initial files for simulation.
${gencase} ${name}_Def ${dirout}/${name} -save:all
if [ $? -ne 0 ] ; then fail; fi

# Executes DualSPHysics to simulate the last 3 seconds.
${dualsphysicscpu} ${dirout}/${name} ${dirout} -dirdataout data -svres -partbegin:100 ${olddiroutdata}
if [ $? -ne 0 ] ; then fail; fi

# Executes post-processing tools for restart simulation...
export dirout2=${dirout}/particles
mkdir ${dirout2}
${partvtk} -dirin ${diroutdata} -savevtk ${dirout2}/PartFluid -onlytype:-all,+fluid
if [ $? -ne 0 ] ; then fail; fi

${partvtkout} -dirin ${diroutdata} -savevtk ${dirout2}/PartFluidOut -SaveResume ${dirout2}/ResumeFluidOut
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/floatings
mkdir ${dirout2}
${boundaryvtk} -loadvtk ${dirout}/${name}__Actual.vtk -motiondata0 ${olddiroutdata} -motiondata ${diroutdata} -savevtkdata ${dirout2}/Floatings.vtk
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/fluidslices
mkdir ${dirout2}
${isosurface} -dirin ${diroutdata} -saveslice ${dirout2}/Slices
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/floatinginfo
mkdir ${dirout2}
${floatinginfo} -dirin ${diroutdata}  -savedata ${dirout2}/FloatingMotion 
if [ $? -ne 0 ] ; then fail; fi

export dirout2=${dirout}/height
mkdir ${dirout2}
${measuretool} -dirin ${diroutdata} -points ${name}_PointsHeights.txt -onlytype:-all,+fluid -height -savevtk ${dirout2}/PointsHeight -savecsv ${dirout2}/_Height
if [ $? -ne 0 ] ; then fail; fi

:success

read -n1 -r -p "Press any key to continue..." key
