#!/usr/bin/env python3

import os
from os import path
import sys
import numpy as np
import glob
import subprocess
import sys
from loguru import logger
sys.path.append(".")
from analysis import analyze, idgen
# from bpy_gen_mandel_stl import idgen
from progress.bar import FillingSquaresBar
## hide all the output
# f = open(os.devnull, 'w')
# sys.stdout = f
logger.remove()
logger.add(sys.stderr, level="TRACE")
# logger.add(sys.stderr, format="{time} {level} {message}", filter="my_module", level="ERROR")
# new_level = logger.level("SNAKY", no=38, color="<yellow>", icon="🐍")
# should really be initializing dbs here
def rm_file(myfile):
    try:
        os.remove(myfile)
    except OSError as e:  ## if failed, report it back to the user ##
        print ("Error: %s - %s." % (e.filename, e.strerror))

rm_file('temp_geo.npy')
rm_file('results.npy')

# this is where the cases come from!
geos2run = np.load('geos1.npy')

# print(geos2run.size)

# try:
#
# except:
#     do_nothing = 0
n_runs = geos2run.shape[0]
print(n_runs)
bar = FillingSquaresBar('Countdown', max = 4*n_runs)
# i don't like that I don't know where I am at any point - hide more stuff and then add some counters!
# is it finally time for a progress bar??
# let's look at the multi-processing pool instead of running in a dumb for loop
for geo in geos2run:
    try:
        rm_file('temp.stl')
    except:
        continue
    logger.debug(str((geo[0],geo[1],geo[2],geo[3],geo[4],geo[5])))
    bulbid = idgen(geo[0],geo[1],geo[2],geo[3],geo[4],geo[5])
    # print(bulbid)
    # check if there is another bulb id the same before running?
    np.save('temp_geo.npy', geo)
    logger.critical(geo)
    ## Create STL
    logger.debug('running blender')
    blender_run_com = '~/blender-2.80-linux-glibc217-x86_64/blender --background --python create_mandel.py'
    blend = subprocess.run(blender_run_com,shell=True,check=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.debug(blend.stdout)
    # check if the new file exists
    if path.exists("temp.stl") == 1:
        continue
    else:
        logger.critical('no stl file generated')
        break
    # check if it is is the same as the old one?
    logger.success('blender stl generation complete')
    logger.success(bar.next())
    ## Run DualSPHysics
    logger.debug('running dualsph')
    dualsph = './xCaseRZ_FlowCylinder3D_linux64_CPU.sh'
    dsph = subprocess.run(dualsph,shell=True,check=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.debug(dsph.stdout)
    logger.success('dualsph run complete')
    logger.critical(bar.next())
    ## Analyze the Case
    list_of_files = glob.glob('mandel_uniform_flow_out/particles/*.vtk') # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    logger.critical(latest_file)
    mesh, results = analyze(latest_file)
    logger.success('results analyzed (and condensed)')
    logger.critical(bar.next())
    mesh.save(bulbid+'.vtk')
    np.save('result_'+bulbid+'.npy',results)
    os.rename('temp.stl','temp2.stl')
    logger.success('mesh and results saved')
    logger.critical(bar.next())
    # logger.log("SNAKY", "Here we go!")
bar.finish()
list_of_files = glob.glob('*.vtk')
print(list_of_files)






## Create STL

## Run DualSPHysics
# os.system('')

## Add Results to NPY file
