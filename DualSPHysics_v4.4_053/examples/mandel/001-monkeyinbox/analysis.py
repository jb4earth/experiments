import pyvista as pv
import numpy as np

def idgen(Va,Vb,Vc,voxels,iters,frac_scale):
    bulbID = ''
    bulbID = str(('exp_'+str(format(Va, '03d'))+
        '_'+str(format(Vb, '03d'))+
        '_'+str(format(Vc, '03d'))+
    '_res_'+str(voxels)+
    '_iters_'+str(iters)+
    '_frac_scale_'+str(frac_scale)
    ))
    return bulbID

def getclipsizes(var_str,mesh,sensor_res):
    mesh.set_active_scalars(var_str)
    s_res = sensor_res
    ii = 0
    nres = np.zeros((s_res*s_res,3))
    for j in np.arange(s_res):
        for k in np.arange(s_res):
            y = ((j/s_res)*2)-1
            z = ((k/s_res)*2)-1

            #'inlet'
            bounds = [-1,-0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][0] = clipped[var_str].size
#             print(clipped[var_str][2]**2)
            # dom
            bounds = [-0.8,0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][1] = clipped[var_str].size

            # outlet
            bounds = [0.8,1 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][2] = clipped[var_str].size

            ii +=1
    results = nres
    return results


def clipndip(var_str,mesh,sensor_res):
    mesh.set_active_scalars(var_str)
    s_res = sensor_res
    ii = 0
    nres = np.zeros((s_res*s_res,6))
    for j in np.arange(s_res):
        for k in np.arange(s_res):
            y = ((j/s_res)*2)-1
            z = ((k/s_res)*2)-1

            #'inlet'
            bounds = [-1,-0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][0] = (clipped[var_str]**2).mean()
            nres[ii][1] = clipped[var_str].std()
#             print(clipped[var_str][2]**2)
            # dom
            bounds = [-0.8,0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][2] = (clipped[var_str]**2).mean()
            nres[ii][3] = clipped[var_str].std()

            # outlet
            bounds = [0.8,1 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][4] = (clipped[var_str]**2).mean()
            nres[ii][5] = clipped[var_str].std()

            ii +=1
    results = nres
    return results

def getclip(var_str,clipped):
    nres = np.zeros(6)
    nres[0] = (clipped[var_str][0]**2).mean()
    nres[1] = (clipped[var_str][1]**2).mean()
    nres[2] = (clipped[var_str][2]**2).mean()
    nres[3] = clipped[var_str][0].std()
    nres[4] = clipped[var_str][1].std()
    nres[5] = clipped[var_str][2].std()
    return nres

def clipn3dip(var_str,mesh,sensor_res):
    mesh.set_active_scalars(var_str)
    s_res = sensor_res
    ii = 0
    nres = np.zeros((s_res*s_res,18))
    for j in np.arange(s_res):
        for k in np.arange(s_res):
            y = ((j/s_res)*2)-1
            z = ((k/s_res)*2)-1

            #'inlet'
            bounds = [-1,-0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][0:6] = getclip(var_str,clipped)

# #             print(clipped[var_str][2]**2)
            # dom
            bounds = [-0.8,0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][6:12] = getclip(var_str,clipped)
#             nres[ii][3] = (clipped[var_str]**2).mean()
#             nres[ii][4] = clipped[var_str].std()


            # outlet
            bounds = [0.8,1 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][12:18] = getclip(var_str,clipped)
#             nres[ii][6] = (clipped[var_str]**2).mean()
#             nres[ii][7] = clipped[var_str].std()


            ii +=1
    results = nres
    return results

def analyze(filename):
    mesh = pv.read(filename)
    results = np.array(getclipsizes('Idp',mesh,3).flatten())
    results = np.hstack((results,(clipndip('Idp',mesh,3).flatten())))
    results = np.hstack((results,(clipndip('Rhop',mesh,3).flatten())))
    results = np.hstack((results,(clipndip('Press',mesh,3).flatten())))
    results = np.hstack((results,(clipndip('Vel',mesh,3).flatten())))
    results = np.hstack((results,(clipndip('Vor',mesh,3).flatten())))
    # results = np.hstack((results,(clipndip('Vel-normed',mesh,3).flatten())))
    results = np.hstack((results,(clipn3dip('Vor',mesh,3).flatten())))
    results = np.hstack((results,(clipn3dip('Vel',mesh,3).flatten())))
    results.shape
    return mesh, results
