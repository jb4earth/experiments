#!/usr/bin/env python3

import os
import sys
import numpy as np
import glob

import sys
sys.path.append(".")
from analysis import analyze, idgen
## hide all the output
# f = open(os.devnull, 'w')
# sys.stdout = f

# this is where the cases come from!
geos2run = np.load('geos0.npy')

print(geos2run.size)

try:
    res = np.load('results.npy')
except:
    do_nothing = 0

# let's look at the multi-processing pool instead of running in a dumb for loop
for geo in geos2run:
    bulbid = idgen(geo[0],geo[1],geo[2],geo[3],geo[4],geo[5])
    print(bulbid)
    # check if there is another bulb id the same before running?
    np.save('temp_geo.npy', geo)
    ## Create STL
    # !! subprocess.run('command',shell=True,check=True)
    os.system('~/blender-2.80-linux-glibc217-x86_64/blender --background --python create_mandel.py')
    ## Run DualSPHysics
    os.system('./xCaseRZ_FlowCylinder3D_linux64_CPU.sh') # i edited this to just delete the previous case without asking
    ## Analyze the Case
    list_of_files = glob.glob('mandel_uniform_flow_out/particles/*.vtk') # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    mesh, results = analyze(latest_file)
    mesh.save(bulbid+'.vtk')
    try:
        res = np.vstack((res,results))
    except:
        res = np.array(results)

np.save('results.npy', res)



## Create STL

## Run DualSPHysics
# os.system('')

## Add Results to NPY file
