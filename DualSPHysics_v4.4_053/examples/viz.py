# analyze results Files

import glob
import numpy as np
from loguru import logger
import sys

# logger.remove()

logger.add(sys.stderr, level="SUCCESS")
logger.add(sys.stderr, level="DEBUG")


rlf = glob.glob('result_e*.npy')
dlf = glob.glob('design_e*.npy')
glf = glob.glob('geo_e*.npy')

print((len(rlf),len(dlf),len(glf)))
