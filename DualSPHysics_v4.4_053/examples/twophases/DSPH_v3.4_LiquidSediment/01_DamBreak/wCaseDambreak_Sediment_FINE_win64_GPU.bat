@echo off
setlocal EnableDelayedExpansion
rem Don't remove the two jump line after than the next line [set NL=^]
set NL=^


rem "name" and "dirout" are named according to the testcase

set name=CaseDambreak_Sediment_FINE
set dirout=%name%_out

rem "executables" are renamed and called from their directory

set dirbin=../../../../bin/windows
set gencase="%dirbin%/GenCase4_win64.exe"
set dsphliquidsediment="%dirbin%/DualSPHysics3.4_LiquidSediment_win64.exe"
set partvtk="%dirbin%/PartVTK4_win64.exe"

:menu
if exist %dirout% ( 
	set /p option="The folder "%dirout%" already exists. Choose an option.!NL!  [1]- Delete it and continue.!NL!  [2]- Execute post-processing.!NL!  [3]- Abort and exit.!NL!"
	if "!option!" == "1" goto run else (
		if "!option!" == "2" goto postprocessing else (
			if "!option!" == "3" goto fail else ( 
				goto menu
			)
		)
	)
)

:run
rem "dirout" is created to store results or it is removed if it already exists

if exist %dirout% rd /s /q %dirout%
mkdir %dirout%
if not "%ERRORLEVEL%" == "0" goto fail

rem CODES are executed according the selected parameters of execution in this tescase

%gencase% %name%_Def %dirout%/%name% -save:all
if not "%ERRORLEVEL%" == "0" goto fail

%dsphliquidsediment% -gpu %dirout%/%name% %dirout% -svres
if not "%ERRORLEVEL%" == "0" goto fail

:postprocessing
set dirout2=%dirout%\particles
mkdir %dirout2%
%partvtk% -dirin %dirout% -savevtk %dirout2%/PartFluid -onlymk:1 -savevtk %dirout2%/PartSediment -onlymk:2
if not "%ERRORLEVEL%" == "0" goto fail


:success
echo All done
goto end

:fail
echo Execution aborted.

:end
pause

