#!/bin/bash

  fail () { 
    echo Execution aborted. 
    read -n1 -r -p "Press any key to continue..." key 
    exit 1 
  }

# "name" and "dirout" are named according to the testcase

name=CaseDambreak_Sediment
dirout=${name}_out

# "executables" are renamed and called from their directory

dirbin=../../../../bin/linux
gencase="${dirbin}/GenCase4_linux64"
dsphliquidsediment="${dirbin}/DualSPHysics3.4_LiquidSediment_linux64"
partvtk="${dirbin}/PartVTK4_linux64"

# Library path must be indicated properly

current=$(pwd)
cd ${dirbin}
path_so=$(pwd)
cd $current
export LD_LIBRARY_PATH=$path_so

option=-1
 if [ -e $dirout ]; then
 while [ "$option" != 1 -a "$option" != 2 -a "$option" != 3 ] 
 do 

	echo -e "The folder "${dirout}" already exists. Choose an option.
  [1]- Delete it and continue.
  [2]- Execute post-processing.
  [3]- Abort and exit.
"
 read -n 1 option 
 done 
  else 
   option=1 
fi 

if [ $option -eq 1 ]; then
	# "dirout" is created to store results or it is cleaned if it already exists

	if [ -e $dirout ]; then
	  rm -f -r $dirout
	fi
	mkdir $dirout

	$gencase ${name}_Def $dirout/$name -save:all
	if [ $? -ne 0 ] ; then fail; fi

	$dsphliquidsediment $dirout/$name $dirout -svres -gpu 
	if [ $? -ne 0 ] ; then fail; fi
fi

if [ $option -eq 2 -o $option -eq 1 ]; then
	dirout2=${dirout}/particles; mkdir $dirout2
	$partvtk -dirin $dirout -savevtk $dirout2/PartFluid -onlymk:1 -savevtk $dirout2/PartSediment -onlymk:2
	if [ $? -ne 0 ] ; then fail; fi
fi

if [ $option != 3 ];then
  echo All done
else
  echo Execution aborted
fi
read -n1 -r -p "Press any key to continue..." key
echo
