#!/bin/bash

  fail () { 
    echo Execution aborted. 
    read -n1 -r -p "Press any key to continue..." key 
    exit 1 
  }

# "name" and "dirout" are named according to the testcase

name=CaseSloshingAcc_LiquidGas
dirout=${name}_out

# "executables" are renamed and called from their directory

dirbin=../../../../bin/linux
gencase="${dirbin}/GenCase4_linux64"
dsphliquidgascpu="${dirbin}/DualSPHysics4.0_LiquidGasCPU_linux64"
dsphliquidgasgpu="${dirbin}/DualSPHysics4.0_LiquidGas_linux64"
boundaryvtk="${dirbin}/BoundaryVTK4_linux64"
partvtk="${dirbin}/PartVTK4_linux64"
partvtkout="${dirbin}/PartVTKOut4_linux64"
measuretool="${dirbin}/MeasureTool4_linux64"
computeforces="${dirbin}/ComputeForces4_linux64"
isosurface="${dirbin}/IsoSurface4_linux64"
flowtool="${dirbin}/FlowTool4_linux64"
floatinginfo="${dirbin}/FloatingInfo4_linux64"

# Library path must be indicated properly

# Library path must be indicated properly

current=$(pwd)
cd ${dirbin}
path_so=$(pwd)
cd $current
export LD_LIBRARY_PATH=$path_so

option=-1
 if [ -e $dirout ]; then
 while [ "$option" != 1 -a "$option" != 2 -a "$option" != 3 ] 
 do 

	echo -e "The folder "${dirout}" already exists. Choose an option.
  [1]- Delete it and continue.
  [2]- Execute post-processing.
  [3]- Abort and exit.
"
 read -n 1 option 
 done 
  else 
   option=1 
fi 
                    
if [ $option -eq 1 ]; then

	if [ -e $dirout ]; then
	    rm -f -r $dirout
	fi
	mkdir $dirout

	# a copy of CaseSloshingAccData.csv must exist in dirout

	cp ${name}_Data.csv $dirout

	$gencase ${name}_Def $dirout/$name -save:all
	if [ $? -ne 0 ] ; then fail; fi

	$dsphliquidgascpu $dirout/$name $dirout -svres
	if [ $? -ne 0 ] ; then fail; fi
fi


if [ $option -eq 2 -o $option -eq 1 ]; then
	dirout2=${dirout}/particles; mkdir $dirout2

	$partvtk -dirin $dirout -savevtk $dirout2/PartFluid -onlytype:-all,+fluid -vars:+press,+mk
	if [ $? -ne 0 ] ; then fail; fi

	$partvtk -dirin $dirout -savevtk $dirout2/PartTank -onlytype:-all,bound
	if [ $? -ne 0 ] ; then fail; fi
	   
	$partvtkout -dirin $dirout -savevtk $dirout2/PartFluidOut -SaveResume $dirout2/_ResumeFluidOut
	if [ $? -ne 0 ] ; then fail; fi
	dirout2=${dirout}/velocity; mkdir $dirout2

	$measuretool -dirin $dirout -points ${name}_PointsPressure.txt -onlytype:-all,+fluid -vars:-all,+press -savevtk $dirout2/PointsPressure_Water -savecsv $dirout2/_PointsPressure_Water -onlymk:1
	if [ $? -ne 0 ] ; then fail; fi
	dirout2=${dirout}/pressure; mkdir $dirout2

	$measuretool -dirin $dirout -points ${name}_PointsPressure.txt -onlytype:-all,+fluid -vars:-all,+press -savevtk $dirout2/PointsPressure_Air -savecsv $dirout2/_PointsPressure_Air -onlymk:2
	if [ $? -ne 0 ] ; then fail; fi
fi

if [ $option != 3 ];then
    echo All done
else
    echo Execution aborted
fi
read -n1 -r -p "Press any key to continue..." key
echo
