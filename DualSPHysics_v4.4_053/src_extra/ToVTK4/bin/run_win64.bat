@echo off
cls

@set dirout=out
if exist %dirout% del /Q %dirout%\*.*
if not exist %dirout% mkdir %dirout%

ToVTK4_win64.exe -dirin Piston2D -filexml Piston2D/Piston2D.xml -savevtk %dirout%/parts -savecsv %dirout%/data


